import { Component, OnInit } from '@angular/core';
import { CatalogService } from '../../services/catalog.service';
import { CategoryModel } from '../../shared/models/category-model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  tempCategoryUrl: string;
  categoryUrl: string;
  model: CategoryModel;
  constructor(private route: ActivatedRoute, private catalogService: CatalogService) {
    this.model = new CategoryModel();
  }
  
  ngOnInit() {
  }
  ngDoCheck() {
    this.categoryUrl = this.route.snapshot.paramMap.get('uriPath');
    if (this.tempCategoryUrl != this.categoryUrl) {
      this.tempCategoryUrl = this.categoryUrl;
      this.catalogService.getCategoryModel(this.categoryUrl).subscribe(x => {
        this.model = x
      });
    }
  }
}
