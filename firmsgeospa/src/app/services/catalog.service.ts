import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CategoryModel } from '../shared/models/category-model';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor(private httpClient: HttpClient) { }
  getCategoryModel(categoryUrl: string): Observable<CategoryModel> {
    var d = this.httpClient.get<CategoryModel>('https://api.firmsgeo.com/api/catalog/category1/' + categoryUrl);
    return d;
  }
}
