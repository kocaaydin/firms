import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CategoryModel } from '../shared/models/category-model';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private httpClient:HttpClient) { }
  getTopMenu():Observable<CategoryModel[]>
  {
    return this.httpClient.get<CategoryModel[]>('https://api.firmsgeo.com/api/default/topmenu');
  }
}
