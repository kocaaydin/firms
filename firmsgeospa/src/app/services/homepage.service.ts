import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HomePageModel } from '../homepage/homepagemodel';

@Injectable({
  providedIn: 'root'
})
export class HomepageService {

  constructor(private httpClient: HttpClient) { }

  getHomePageModel(): Observable<HomePageModel> {
    return this.httpClient.get<HomePageModel>('https://api.firmsgeo.com/api/default/index');
  }
}
