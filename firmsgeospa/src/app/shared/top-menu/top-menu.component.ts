import { Component, OnInit } from '@angular/core';
import { CategoryModel } from '../models/category-model';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css']
})
export class TopMenuComponent implements OnInit {
  model: CategoryModel[];
  constructor(private commonService: CommonService) { }

  ngOnInit() {
    this.commonService.getTopMenu().subscribe(x => {
      this.model = x;
    });
  }
}
