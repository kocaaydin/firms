import { BreadcrumbModel } from "./breadcrumb-model";
import { FilterModel } from "./filter-model";

export class CategoryModel {
    
    public get urlPrefix() : string {
        return '/catalog/category';
    }
    

    url: string;
    name: string;
    metaDescription?: any;
    metaKeywords?: any;
    image: string;
    includeTopMenu: boolean;
    parentCategoryId: number;
    companies: any[];
    filterModel: FilterModel;
    breadcrumb: BreadcrumbModel;
    id: number;
}
