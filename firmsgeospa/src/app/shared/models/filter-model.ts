export class FilterModel {
    pageIndex: number;
    pageNumber: number;
    pageSize: number;
    totalItems: number;
    totalPages: number;
    firstItem: number;
    lastItem: number;
    hasPreviousPage: boolean;
    hasNextPage: boolean;
    id: number;
}
