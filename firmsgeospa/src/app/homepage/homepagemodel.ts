import { CompanyModel } from "../shared/models/company-model";
import { CategoryModel } from "../shared/models/category-model";
export class HomePageModel {
    recentlyCompanies: CompanyModel[];
    popularCompaines: CompanyModel[];
    homePageCategories: CategoryModel[];
    categoryCount: number;
    companyCount: number;
    hostName: string;
    description:string;
    title:string;
}