import { Component, OnInit } from '@angular/core';
import { HomePageModel } from './homepagemodel';
import { HomepageService } from '../services/homepage.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  model: HomePageModel;
  constructor(private homePageService: HomepageService) {
    this.model = new HomePageModel();
  }

  ngOnInit() {
    this.homePageService.getHomePageModel().subscribe(x => {
      this.model = x
    });
  }
}